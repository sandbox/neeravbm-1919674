<?php

/**
 * Admin configuration form.
 */
function email_analytics_admin_config_form($form) {
	$form = array();

	$module_keys = db_select('email_analytics_module_keys', 'k')
		->fields('k', array('email_module_key'))
		->execute()
		->fetchCol();

	$form['email_module_keys'] = array(
		'#type' => 'textarea',
		'#title' => t('Email module keys'),
		'#description' => t('Provide module keys of all emails for which you want to capture analytics. Enter each key id on a new line.'),
		'#default_value' => implode("\n", $module_keys),
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);

	return $form;
}

/**
 * Validation callback for admin configuration form.
 */
function email_analytics_admin_config_form_validate($form, &$form_state) {
	$email_module_keys_area = check_plain($form_state['values']['email_module_keys']);
	$email_module_keys = explode("\r", $email_module_keys_area);

	$new_email_module_keys = array();
	foreach ($email_module_keys as $email_module_key) {
		if (!empty($email_module_key)) {
			$new_email_module_keys[] = $email_module_key;
		}
	}

	$form_state['values']['email_module_keys'] = $new_email_module_keys;
}

/**
 * Submit callback for admin configuration form.
 */
function email_analytics_admin_config_form_submit($form, &$form_state) {
	$email_module_keys = $form_state['values']['email_module_keys'];

	$trimmed_email_module_keys = array();
	foreach ($email_module_keys as $email_module_key) {
		$email_module_key = trim($email_module_key);

		db_merge('email_analytics_module_keys')
			->key(array('email_module_key' => $email_module_key))
			->execute();

		$trimmed_email_module_keys[] = $email_module_key;
	}

	// Now delete all the keys that are not in email_ids.
	$result = db_delete('email_analytics_module_keys')
		->condition('email_module_key', $trimmed_email_module_keys, 'NOT IN')
		->execute();

	drupal_set_message(t('Your changes have been saved.'));
}

/**
 * Returns the default header.
 */
function email_analytics_main_report_default_header() {
	$header = array(
		array(
			'data' => t('Sent'),
			'field' => 'created',
			'sort' => 'desc',
			'weight' => -45,
		),
		array(
			'data' => t('To'),
			'field' => 'email_to',
			'sort' => 'asc',
			'weight' => -35,
		),
		array(
			'data' => t('Uid'),
			'key' => 'uid',
			'weight' => -25,
		),
		array(
			'data' => t('Subject'),
			'field' => 'subject',
			'sort' => 'asc',
			'weight' => -18,
		),
		array(
			'data' => t('Opened'),
			'field' => 'opened',
			'sort' => 'desc',
			'weight' => -5,
		),
		array(
			'data' => t('Clicked'),
			'field' => 'clicked',
			'sort' => 'desc',
			'weight' => 5,
		),
	);

	return $header;
}

/**
 * View email analytics data.
 */
function email_analytics_view() {

	$header = email_analytics_main_report_default_header();
	drupal_alter('email_analytics_main_report_header', $header);

	$query = email_analytics_build_main_report_query($header, 50);
	$result = $query->execute();
	$results = array();
	foreach ($result as $record) {
		$results[] = $record;
	}

	$output = drupal_render(drupal_get_form('email_analytics_main_report_filter_form', $results));
	$output .= theme('email_analytics_main_report', array('result' => $results, 'header' => $header));

	$pager = array('#theme' => 'pager');
	$output .= drupal_render($pager);

	return $output;
}

/**
 * Builds email analytics query for main report.
 */
function email_analytics_build_main_report_query($header = NULL, $limit = NULL, $tag = 'email_analytics_main_report') {
	$query = db_select('email_analytics_info', 'eai')
		->fields('eai', array('eid', 'email_to', 'uid', 'email_hash', 'module_key', 'subject', 'created', 'entity_id', 'data'));
		//->orderBy('eai.created', 'DESC')
		//->groupBy('eai.eid');

	if (!empty($_SESSION['email_analytics_main_report']['sent_to'])) {
		$query->condition('eai.email_to', $_SESSION['email_analytics_main_report']['sent_to']);
	}

	if (!empty($_SESSION['email_analytics_main_report']['uid'])) {
		$query->condition('eai.uid', $_SESSION['email_analytics_main_report']['uid']);
	}

	if (!empty($_SESSION['email_analytics_main_report']['min_date'])) {
		$query->condition('eai.created', $_SESSION['email_analytics_main_report']['min_date'], '>=');
	}

	if (!empty($_SESSION['email_analytics_main_report']['max_date'])) {
		$query->condition('eai.created', $_SESSION['email_analytics_main_report']['max_date'], '<=');
	}

	// Extend this query using a pager.
	if (!is_null($limit)) {
		$query = $query->extend('PagerDefault')->limit($limit);
	}

	$open_action_table_query = db_select('email_analytics_data', 'eado')
		->condition('eado.action', 'open')
		->groupBy('eado.eid')
		->addTag('email_analytics_open_action_main_report');
	$open_eid_field = $open_action_table_query->addField('eado', 'eid', 'open_eid');
	$open_created_field = $open_action_table_query->addExpression('MIN(eado.created)', 'open_created');

	$click_action_table_query = db_select('email_analytics_data', 'eadc')
		->condition('eadc.action', 'click')
		->groupBy('eadc.eid')
		->addTag('email_analytics_click_action_main_report');
	$click_eid_field = $click_action_table_query->addField('eadc', 'eid', 'click_eid');
	$click_created_field = $click_action_table_query->addExpression('MIN(eadc.created)', 'click_created');

	$open_action_table_alias = $query->leftJoin($open_action_table_query, 'eado', 'eai.eid = eado.' . $open_eid_field);
	$click_action_table_alias = $query->leftJoin($click_action_table_query, 'eadc', 'eai.eid = eadc.' . $click_eid_field);

	$main_table_open_field = $query->addField('eai', $open_action_table_alias . '.' . $open_created_field, 'opened');
	$main_table_click_field = $query->addField('eai', $click_action_table_alias . '.' . $click_created_field, 'clicked');

	// Add TableSort extender.
	if (!is_null($header)) {
		$query = $query->extend('TableSort')->orderByHeader($header);
	}

	// Add a tag so that other modules can modify this query.
	$query->addTag($tag);

	return $query;
}

/**
 * Renders the main report of email analytics data.
 */
function theme_email_analytics_main_report($variables) {

	$result = $variables['result'];
	$header = $variables['header'];
	$format_csv = $variables['format_csv'];

	// If header is not supplied, get the default header.
	if (is_null($header)) {
		$header = email_analytics_main_report_default_header();
		drupal_alter('email_analytics_main_report_header', $header);
	}

	// Iterate through the header and keep only the ones which either don't have any access value or access value is set to TRUE.
	$visible_header = array();
	foreach ($header as $key => $header_cell) {
		if (!isset($header_cell['access'])) {
			$visible_header[] = $header[$key];
		}
		elseif ($header_cell['access']) {
			$visible_header[] = $header[$key];
		}
	}
	uasort($visible_header, 'drupal_sort_weight');

	$rows = array();
	foreach ($result as $record) {
		$row = array();

		// Go through the header and create rows from displayed fields.
		foreach ($visible_header as $header_cell) {
			$field = NULL;
			if (!empty($header_cell['field'])) {
				$field = $header_cell['field'];
			}
			elseif (!empty($header_cell['key'])) {
				$field = $header_cell['key'];
			}

			if (is_null($field)) {
				$row[] = NULL;
				continue;
			}

			if (empty($record->$field)) {
				$row[] = NULL;
				continue;
			}

			$row[] = array(
				'data' => $record->$field,
			);
		}

		$rows[] = $row;
	}

	if (module_exists('format_csv') && $format_csv) {
		return theme('format_csv', array(
			'header' => $visible_header,
			'rows' => $rows,
			'sticky' => TRUE,
			'empty' => t('No email analytics data found.'),
		));
	}

	return theme('table', array(
		'header' => $visible_header,
		'rows' => $rows,
		'sticky' => TRUE,
		'empty' => t('No email analytics data found.'),
	));
}

/**
 * Creates a filter form for the email analytics main report.
 */
function email_analytics_main_report_filter_form($form, $form_state, $result) {
	$form = array();

	$form['filter'] = array(
		'#type' => 'fieldset',
		'#title' => t('Filter by'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#attached' => array(
			'css' => array(
				drupal_get_path('module', 'email_analytics') . '/css/email_analytics.filters.css',
			),
		),
	);

	// Build filters. We don't want to supply $result here since the filters need to be dependent on all the rows of result, not just the ones shown by the pager.
	$form['filter']['filters'] = email_analytics_main_report_filters();

	$form['filter']['result'] = array(
		'#type' => 'value',
		'#value' => $result,
	);

	$form['filter']['actions'] = array(
		'submit' => array(
			'#type' => 'submit',
			'#value' => t('Submit'),
			'#name' => 'submit',
		),
		'reset' => array(
			'#type' => 'submit',
			'#value' => t('Reset'),
			'#name' => 'reset',
		),
	);

	$form['filter']['download'] = array(
		'#type' => 'link',
		'#href' => 'admin/reports/email_analytics/individual/csv_download',
		'#title' => t('Download as csv file'),
	);

	return $form;
}

/**
 * Submit handler for the filter form.
 */
function email_analytics_main_report_filter_form_submit($form, &$form_state) {
	if ($form_state['triggering_element']['#name'] == 'submit') {
		// Save the values in $_SESSION.
		if (!empty($form_state['values']['sent_to'])) {
			$_SESSION['email_analytics_main_report']['sent_to'] = $form_state['values']['sent_to'];
		}
		else {
			unset($_SESSION['email_analytics_main_report']['sent_to']);
		}

		if (!empty($form_state['values']['uid'])) {
			$_SESSION['email_analytics_main_report']['uid'] = $form_state['values']['uid'];
		}
		else {
			unset($_SESSION['email_analytics_main_report']['uid']);
		}

		if (!empty($form_state['values']['min_date'])) {
			$minDate = new DateTime($form_state['values']['min_date']);
			$_SESSION['email_analytics_main_report']['min_date'] = $minDate->getTimestamp();
		}
		else {
			unset($_SESSION['email_analytics_main_report']['min_date']);
		}

		if (!empty($form_state['values']['max_date'])) {
			$maxDate = new DateTime($form_state['values']['max_date']);
			$_SESSION['email_analytics_main_report']['max_date'] = $maxDate->getTimestamp() + (24 * 60 * 60 - 1);
		}
		else {
			unset($_SESSION['email_analytics_main_report']['max_date']);
		}
	}
	elseif ($form_state['triggering_element']['#name'] == 'reset') {
		unset($_SESSION['email_analytics_main_report']);
	}
}

/**
 * List email analytics administration filters that can be applied.
 */
function email_analytics_main_report_filters($result = NULL) {

	if (is_null($result)) {
		// Find all the possible results.
		$query = email_analytics_build_main_report_query();
		$result = $query->execute();
	}

	// Get email address, uids and date filters from this list.
	$email_addresses = array();
	$uids = array();
	$min_date = NULL;
	$max_date = NULL;
	foreach ($result as $record) {
		if (!is_null($record->uid)) {
			$uids[] = $record->uid;
		}

		if (!in_array($record->email_to, $email_addresses)) {
			$email_addresses[] = $record->email_to;
		}

		$min_date = min($min_date, $record->created);
		$max_date = max($max_date, $record->created);
	}

	$filters = array();

	$filters['sent_to'] = array(
		'#type' => 'select',
		'#title' => t('Email address'),
		'#options' => drupal_map_assoc($email_addresses),
		'#empty_option' => '- None -',
		'#weigth' => -25,
	);
	if (!empty($_SESSION['email_analytics_main_report']['sent_to'])) {
		$filters['sent_to']['#default_value'] = $_SESSION['email_analytics_main_report']['sent_to'];
	}

	$filters['uid'] = array(
		'#type' => 'select',
		'#title' => t('Uid'),
		'#options' => drupal_map_assoc($uids),
		'#empty_option' => '- None -',
		'#weight' => -15,
	);
	if (!empty($_SESSION['email_analytics_main_report']['uid'])) {
		$filters['uid']['#default_value'] = $_SESSION['email_analytics_main_report']['uid'];
	}

	if (module_exists('date_popup')) {
		$filters['min_date'] = array(
			'#type' => 'date_popup',
			'#title' => t('Emails sent after'),
			'#date_type' => DATE_UNIX,
			'#date_format' => 'Y/m/d',
			'#date_increment' => 1,
			'#weight' => -5,
		);
		if (!empty($_SESSION['email_analytics_main_report']['min_date'])) {
			$filters['min_date']['#default_value'] = format_date($_SESSION['email_analytics_main_report']['min_date'], 'custom', 'Y-m-d');
		}

		$filters['max_date'] = array(
			'#type' => 'date_popup',
			'#date_type' => DATE_UNIX,
			'#date_increment' => 1,
			'#title' => t('Emails sent before'),
			'#date_format' => 'Y/m/d',
			'#weight' => 5,
		);
		if (!empty($_SESSION['email_analytics_main_report']['max_date'])) {
			$filters['max_date']['#default_value'] = format_date($_SESSION['email_analytics_main_report']['max_date'], 'custom', 'Y-m-d');
		}
	}

	return $filters;
}

/**
 * Returns the aggregate data by email address.
 */
function email_analytics_aggregate_view() {
	$header = email_analytics_aggregate_report_default_header();
	drupal_alter('email_analytics_aggregate_report_header', $header);

	$query = email_analytics_build_aggregate_report_query($header, 50);
	$result = $query->execute();
	$results = array();
	foreach ($result as $record) {
		$results[] = $record;
	}

	$output = drupal_render(drupal_get_form('email_analytics_aggregate_report_filter_form', $results));
	$output .= theme('email_analytics_aggregate_report', array('result' => $results, 'header' => $header));

	$pager = array('#theme' => 'pager');
	$output .= drupal_render($pager);

	return $output;
}

/**
 * Returns the default aggregate header.
 */
function email_analytics_aggregate_report_default_header() {
	$header = array(
		array(
			'data' => t('To'),
			'field' => 'email_to',
			'sort' => 'asc',
			'weight' => -35,
		),
		array(
			'data' => t('Uid'),
			'key' => 'uid',
			'weight' => -25,
		),
	);

	$days = array();
	if (!empty($_SESSION['email_analytics_aggregate_report']['days'])) {
		$days = $_SESSION['email_analytics_aggregate_report']['days'];
	}

	$index = 0;
	foreach ($days as $day) {
		$header[] = array(
			'data' => t('Emails sent in last @day days', array('@day' => $day)),
			'field' => 'sent_count_' . check_plain($day),
			'sort' => 'desc',
			'weight' => 5 * $index + 1,
		);
		$header[] = array(
			'data' => t('Emails opened in last @day days', array('@day' => $day)),
			'field' => 'open_count_' . check_plain($day),
			'sort' => 'desc',
			'weight' => 5 * $index + 2,
		);
		$header[] = array(
			'data' => t('Emails clicked in last @day days', array('@day' => $day)),
			'field' => 'click_count_' . check_plain($day),
			'sort' => 'desc',
			'weight' => 5 * $index + 3,
		);
		$index++;
	}

	$header[] = array(
		'data' => t('Total emails sent'),
		'field' => 'sent_count_total',
		'sort' => 'desc',
		'weight' => 45,
	);
	$header[] = array(
		'data' => t('Total emails opened'),
		'field' => 'open_count_total',
		'sort' => 'desc',
		'weight' => 46,
	);
	$header[] = array(
		'data' => t('Total emails clicked'),
		'field' => 'click_count_total',
		'sort' => 'desc',
		'weight' => 47,
	);

	return $header;
}

/**
 * Builds aggregate query for one day period.
 */
function email_analytics_build_day_query($day = NULL) {

	$query = db_select('email_analytics_info', 'eai')
		->fields('eai', array('eid', 'email_to', 'uid', 'email_hash', 'module_key', 'subject', 'created', 'entity_id', 'data'))
		->addTag('email_analytics_day')
		->addMetaData('day', $day);

	if (!is_null($day)) {
		$now = time();
		$query->condition('eai.created', ($now - $day * 24 * 3600), '>=');
	}

	if (!empty($_SESSION['email_analytics_aggregate_report']['sent_to'])) {
		$query->condition('eai.email_to', $_SESSION['email_analytics_aggregate_report']['sent_to']);
	}

	if (!empty($_SESSION['email_analytics_aggregate_report']['uid'])) {
		$query->condition('eai.uid', $_SESSION['email_analytics_aggregate_report']['uid']);
	}

	$open_action_table_query = db_select('email_analytics_data', 'eado')
		->condition('eado.action', 'open')
		->groupBy('eado.eid')
		->addTag('email_analytics_action_day')
		->addMetaData('day', $day)
		->addMetaData('action', 'open');
	$open_eid_field = $open_action_table_query->addField('eado', 'eid', 'open_eid');
	$open_created_field = $open_action_table_query->addExpression('MIN(eado.created)', 'open_created');

	$click_action_table_query = db_select('email_analytics_data', 'eadc')
		->condition('eadc.action', 'click')
		->groupBy('eadc.eid')
		->addTag('email_analytics_action_day')
		->addMetaData('day', $day)
		->addMetaData('action', 'click');
	$click_eid_field = $click_action_table_query->addField('eadc', 'eid', 'click_eid');
	$click_created_field = $click_action_table_query->addExpression('MIN(eadc.created)', 'click_created');

	$open_action_table_alias = $query->leftJoin($open_action_table_query, 'eado', 'eai.eid = eado.' . $open_eid_field);
	$click_action_table_alias = $query->leftJoin($click_action_table_query, 'eadc', 'eai.eid = eadc.' . $click_eid_field);

	$sent_action_table_count_field = $query->addExpression('COUNT(eai.eid)', 'sent');
	$open_action_table_count_field = $query->addExpression('COUNT(open_created)', 'opened');
	$click_action_table_count_field = $query->addExpression('COUNT(click_created)', 'clicked');

	$query->groupBy('email_to');

	return $query;
}

/**
 * Build aggregate query.
 */
function email_analytics_build_aggregate_report_query($header = NULL, $limit = NULL, $tag = 'email_analytics_aggregate_report') {

	/*$query = db_select('email_analytics_info', 'eai')
		->fields('eai', array('eid', 'email_to', 'uid', 'email_hash', 'module_key', 'subject', 'created', 'entity_id', 'data'));
		
	if (!empty($_SESSION['email_analytics_aggregate_report']['sent_to'])) {
		$query->condition('eai.email_to', $_SESSION['email_analytics_aggregate_report']['sent_to']);
	}

	if (!empty($_SESSION['email_analytics_aggregate_report']['uid'])) {
		$query->condition('eai.uid', $_SESSION['email_analytics_aggregate_report']['uid']);
	}

	$query->groupBy('eai.email_to');*/


	// Left Join with total days query.
	$total_query = email_analytics_build_day_query(NULL);
	$query = db_select($total_query, 'total_query');
	$total_email_to_field = $query->addField('total_query', 'email_to', 'email_to');
	$total_uid_field = $query->addField('total_query', 'uid', 'uid');
	$total_sent_field = $query->addField('total_query', 'sent', 'sent_count_total');
	$total_opened_field = $query->addField('total_query', 'opened', 'open_count_total');
	$total_clicked_field = $query->addField('total_query', 'clicked', 'click_count_total');

	// Extend this query using a pager.
	if (!is_null($limit)) {
		$query = $query->extend('PagerDefault')->limit($limit);
	}

	$days = array();
	if (!empty($_SESSION['email_analytics_aggregate_report']['days'])) {
		$days = $_SESSION['email_analytics_aggregate_report']['days'];
	}

	foreach ($days as $day) {
		$day_query = email_analytics_build_day_query($day);
		$day_table_alias = $query->leftJoin($day_query, 'day_query_' . $day, 'total_query.email_to = day_query_' . $day . '.email_to');
		$day_sent_field = $query->addField($day_table_alias, 'sent', 'sent_count_' . $day);
		$day_opened_field = $query->addField($day_table_alias, 'opened', 'open_count_' . $day);
		$day_clicked_field = $query->addField($day_table_alias, 'clicked', 'click_count_' . $day);
	}

	// Add TableSort extender.
	if (!is_null($header)) {
		$query = $query->extend('TableSort')->orderByHeader($header);
	}

	// Add a tag so that other modules can modify this query.
	$query->addTag($tag);

	return $query;
}

/**
 * Renders the aggregate report of email analytics data.
 */
function theme_email_analytics_aggregate_report($variables) {

	$result = $variables['result'];
	$header = $variables['header'];
	$format_csv = $variables['format_csv'];

	// If header is not supplied, get the default header.
	if (is_null($header)) {
		$header = email_analytics_aggregate_report_default_header();
		drupal_alter('email_analytics_aggregate_report_header', $header);
	}

	// Iterate through the header and keep only the ones which either don't have any access value or access value is set to TRUE.
	$visible_header = array();
	foreach ($header as $key => $header_cell) {
		if (!isset($header_cell['access'])) {
			$visible_header[] = $header[$key];
		}
		elseif ($header_cell['access']) {
			$visible_header[] = $header[$key];
		}
	}
	uasort($visible_header, 'drupal_sort_weight');

	$rows = array();
	foreach ($result as $record) {
		$row = array();

		// Go through the header and create rows from displayed fields.
		foreach ($visible_header as $header_cell) {
			$field = NULL;
			if (!empty($header_cell['field'])) {
				$field = $header_cell['field'];
			}
			elseif (!empty($header_cell['key'])) {
				$field = $header_cell['key'];
			}

			if (is_null($field)) {
				$row[] = NULL;
				continue;
			}

			if (empty($record->$field)) {
				$row[] = NULL;
				continue;
			}

			$row[] = array(
				'data' => $record->$field,
			);
		}

		$rows[] = $row;
	}

	if (module_exists('format_csv') && $format_csv) {
		return theme('format_csv', array(
			'header' => $visible_header,
			'rows' => $rows,
			'sticky' => TRUE,
			'empty' => t('No email analytics data found.'),
		));
	}

	return theme('table', array(
		'header' => $visible_header,
		'rows' => $rows,
		'sticky' => TRUE,
		'empty' => t('No email analytics data found.'),
	));
}

/**
 * Creates a filter form for the email analytics aggregate report.
 */
function email_analytics_aggregate_report_filter_form($form, $form_state, $result) {
	$form = array();

	$form['filter'] = array(
		'#type' => 'fieldset',
		'#title' => t('Filter by'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#attached' => array(
			'css' => array(
				drupal_get_path('module', 'email_analytics') . '/css/email_analytics.filters.css',
			),
		),
	);

	// Build filters. We don't want to supply $result here since the filters need to be dependent on all the rows of result, not just the ones shown by the pager.
	$form['filter']['filters'] = email_analytics_aggregate_report_filters();

	$form['filter']['result'] = array(
		'#type' => 'value',
		'#value' => $result,
	);

	$form['filter']['actions'] = array(
		'submit' => array(
			'#type' => 'submit',
			'#value' => t('Submit'),
			'#name' => 'submit',
		),
		'reset' => array(
			'#type' => 'submit',
			'#value' => t('Reset'),
			'#name' => 'reset',
		),
	);

	$form['filter']['download'] = array(
		'#type' => 'link',
		'#href' => 'admin/reports/email_analytics/aggregate/csv_download',
		'#title' => t('Download as csv file'),
	);

	return $form;
}

/**
 * List email analytics administration filters that can be applied.
 */
function email_analytics_aggregate_report_filters($result = NULL) {

	if (is_null($result)) {
		// Find all the possible results.
		$query = email_analytics_build_aggregate_report_query();
		$result = $query->execute();
	}

	// Get email address, uids and date filters from this list.
	$email_addresses = array();
	$uids = array();
	foreach ($result as $record) {
		if (!is_null($record->uid)) {
			$uids[] = $record->uid;
		}

		if (!in_array($record->email_to, $email_addresses)) {
			$email_addresses[] = $record->email_to;
		}
	}

	$filters = array();

	$filters['sent_to'] = array(
		'#type' => 'select',
		'#title' => t('Email address'),
		'#options' => drupal_map_assoc($email_addresses),
		'#empty_option' => '- None -',
		'#weigth' => -25,
	);
	if (!empty($_SESSION['email_analytics_aggregate_report']['sent_to'])) {
		$filters['sent_to']['#default_value'] = $_SESSION['email_analytics_aggregate_report']['sent_to'];
	}

	$filters['uid'] = array(
		'#type' => 'select',
		'#title' => t('Uid'),
		'#options' => drupal_map_assoc($uids),
		'#empty_option' => '- None -',
		'#weight' => -15,
	);
	if (!empty($_SESSION['email_analytics_aggregate_report']['uid'])) {
		$filters['uid']['#default_value'] = $_SESSION['email_analytics_aggregate_report']['uid'];
	}

	$filters['days'] = array(
		'#type' => 'textfield',
		'#size' => 15,
		'#title' => t('Days'),
		'#default_value' => (!empty($_SESSION['email_analytics_aggregate_report']['days']) ? implode(',', $_SESSION['email_analytics_aggregate_report']['days']) : ''),
		'#description' => t('Comma separated list of days. For e.g. 7,30'),
	);

	return $filters;
}

/**
 * Validate handler for the aggregate report filter form.
 */
function email_analytics_aggregate_report_filter_form_validate($form, &$form_state) {
	// Make sure that field in days are comma-separated integers greater than 0.
	$day_field = check_plain($form_state['values']['days']);
	$day_field_array = explode(',', $day_field);
	$days = array();
	foreach ($day_field_array as $day_field_value) {
		$day_fiel_value = trim($day_field_value);
		if (!empty($day_field_value)) {
			if (is_numeric($day_field_value)) {
				if ($day_field_value > 0) {
					$days[] = $day_field_value;
				}
				else {
					form_set_error('days', t('Please provide a comma-separated list of integers greater than 0.'));
				}
			}
			else {
				form_set_error('days', t('Please provide a comma-separated list of integers.'));
			}
		}
	}
	$form_state['values']['days'] = $days;
}

/**
 * Submit handler for the filter form.
 */
function email_analytics_aggregate_report_filter_form_submit($form, &$form_state) {
	if ($form_state['triggering_element']['#name'] == 'submit') {
		// Save the values in $_SESSION.
		if (!empty($form_state['values']['sent_to'])) {
			$_SESSION['email_analytics_aggregate_report']['sent_to'] = $form_state['values']['sent_to'];
		}
		else {
			unset($_SESSION['email_analytics_aggregate_report']['sent_to']);
		}

		if (!empty($form_state['values']['uid'])) {
			$_SESSION['email_analytics_aggregate_report']['uid'] = $form_state['values']['uid'];
		}
		else {
			unset($_SESSION['email_analytics_aggregate_report']['uid']);
		}

		if (!empty($form_state['values']['days'])) {
			$_SESSION['email_analytics_aggregate_report']['days'] = $form_state['values']['days'];
		}
		else {
			unset($_SESSION['email_analytics_aggregate_report']['days']);
		}
	}
	elseif ($form_state['triggering_element']['#name'] == 'reset') {
		unset($_SESSION['email_analytics_aggregate_report']);
	}
}

/**
 * Callback to download analytics data in csv file.
 */
function email_analytics_csv_download($type) {

	$table = '';
	if ($type == 'individual') {
		$header = email_analytics_main_report_default_header();
		drupal_alter('email_analytics_main_report_header', $header);

		$query = email_analytics_build_main_report_query($header);
		$result = $query->execute();
		$results = array();
		foreach ($result as $record) {
			$results[] = $record;
		}

		$table = theme('email_analytics_main_report', array('result' => $results, 'header' => $header, 'format_csv' => TRUE));
	}
	elseif ($type == 'aggregate') {
		$header = email_analytics_aggregate_report_default_header();
		drupal_alter('email_analytics_aggregate_report_header', $header);

		$query = email_analytics_build_aggregate_report_query($header);
		$result = $query->execute();
		$results = array();
		foreach ($result as $record) {
			$results[] = $record;
		}

		$table = theme('email_analytics_aggregate_report', array('result' => $results, 'header' => $header, 'format_csv' => TRUE));
	}
	else {
		drupal_not_found();
	}

	drupal_add_http_header('Content-type', 'application/ms-excel');
	drupal_add_http_header('Content-Disposition', 'attachment; filename="email_tracking_' . $type . '.csv"');

	$fp = fopen("php://output", 'w');

	print $table;

	fclose($fp);
}

/**
 * View detailed statistics about an email sent.
 */
function email_analytics_view_details($eid) {
	if (!is_numeric($eid)) {
		drupal_not_found();
	}

	$result = db_select('email_analytics_data', 'ead')
		->condition('ead.eid', $eid)
		->fields('ead', array('created', 'action', 'url', 'page_title'))
		->execute();

	$header = array(
		t('Date & Time'),
		t('Action'),
		t('URL'),
		t('Page title'),
	);

	$rows = array();
	foreach ($result as $record) {
		$row = array(
			format_date($record->created, 'short'),
			$record->action
		);

		if (!empty($record->url)) {
			$row[] = l($record->url, ltrim($record->url, "/"));
			$row[] = l($record->page_title, ltrim($record->url, "/"));
		}
		else {
			$row[] = '';
			$row[] = '';
		}

		$rows[] = $row;
	}

	return theme('table', array(
		'header' => $header,
		'rows' => $rows,
		'sticky' => TRUE,
		'empty' => t('No action has been taken on this email.'),
	));
}
